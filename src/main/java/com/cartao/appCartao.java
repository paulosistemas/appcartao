package com.cartao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class appCartao {
    public static void main(String[] args) {
        SpringApplication.run(appCartao.class, args);
    }
}
