package com.cartao.enums;

public enum SituacaoCartao {
    Ativo,
    Inativo
}
