package com.cartao.services;

import com.cartao.models.Transacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cartao.repositories.TransacaoRepository;

@Service
public class TransacaoService {

    @Autowired
    TransacaoRepository transacaoRepository;

    public Transacao salvarTransacao(Transacao transacao){
        return transacaoRepository.save(transacao);
    }
}
