package com.cartao.services;

import com.cartao.enums.SituacaoCartao;
import com.cartao.models.Cartao;
import com.cartao.models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cartao.repositories.CartaoRepository;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    public Cartao salvarCartao(Cartao cartao){

        cartao.setSituacaoCartao(SituacaoCartao.Inativo);
        return cartaoRepository.save(cartao);
    }

    public Cartao buscarCartaoPorId(int idCartao) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(idCartao);
        if (optionalCartao.isPresent()) {
            return optionalCartao.get();
        }
        throw new RuntimeException("Cartao não encontrado, ID informado:" + idCartao);
    }

    public Cartao ativarCartao(int idCartao){
        Cartao cartao = buscarCartaoPorId(idCartao);
        cartao.setSituacaoCartao(SituacaoCartao.Ativo);
        cartaoRepository.save(cartao);
        return cartao;
    }

}
