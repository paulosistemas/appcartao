package com.cartao.services;

import com.cartao.models.Cliente;
import com.cartao.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente buscarClientePorId(int idCliente){
        Optional<Cliente> optionalCliente = clienteRepository.findById(idCliente);
        if (optionalCliente.isPresent()) {
            return optionalCliente.get();
        }
        throw  new RuntimeException("Cliente não encontrado, ID informado:" + idCliente);
    }

}
