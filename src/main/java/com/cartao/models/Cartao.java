package com.cartao.models;

import com.cartao.enums.*;
import com.cartao.models.Cliente;
import javax.persistence.*;

@Entity
public class Cartao {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int idCartao;

    private String numeroCartao;

    private SituacaoCartao situacaoCartao;

    @ManyToOne
    @JoinColumn(name = "id_Cliente", nullable = false)
    private Cliente cliente;

    public int getIdCartao() {
        return idCartao;
    }

    public void setSituacaoCartao(com.cartao.enums.SituacaoCartao situacaoCartao) {
        this.situacaoCartao = situacaoCartao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public SituacaoCartao getSituacaoCartao() {
        return situacaoCartao;
    }

}
