package com.cartao.models;

import javax.persistence.*;

@Entity
public class Transacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTransacao;

    private String descricaoTransacao;

    private double valorTransacao;

    @ManyToOne
    @JoinColumn(name = "id_Cartao", nullable = false)
    private Cartao cartao;

    public int getIdTransacao() {
        return idTransacao;
    }

    public void setIdTransacao(int idTransacao) {
        this.idTransacao = idTransacao;
    }

    public String getDescricaoTransacao() {
        return descricaoTransacao;
    }

    public void setDescricaoTransacao(String descricaoTransacao) {
        this.descricaoTransacao = descricaoTransacao;
    }

    public double getValorTransacao() {
        return valorTransacao;
    }

    public void setValorTransacao(double valorTransacao) {
        this.valorTransacao = valorTransacao;
    }

    public Cartao getIdCartao() {
        return cartao;
    }

    public void setIdCartao(Cartao cartao) {
        this.cartao = cartao;
    }
}
