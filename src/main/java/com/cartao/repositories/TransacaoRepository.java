package com.cartao.repositories;

import com.cartao.models.Transacao;
import org.springframework.data.repository.CrudRepository;

public interface TransacaoRepository extends CrudRepository<Transacao, Integer> {
}
