package com.cartao.controllers;

import com.cartao.models.DTO.PagamentoDTO;
import com.cartao.models.Transacao;
import com.cartao.services.CartaoService;
import com.cartao.services.TransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    TransacaoService transacaoService;

    @Autowired
    CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Transacao efetuaPagamento(@RequestBody PagamentoDTO pagamentoDTO){
        Transacao transacao = new Transacao();

        int idCartao = pagamentoDTO.getIdCartao();

        transacao.setIdCartao(cartaoService.buscarCartaoPorId(idCartao));
        transacao.setDescricaoTransacao(pagamentoDTO.getDescricao());
        transacao.setValorTransacao(pagamentoDTO.getValor());
        return transacaoService.salvarTransacao(transacao);

    }

}
